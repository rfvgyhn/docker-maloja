FROM python:3-alpine

ARG MALOJA_RELEASE

LABEL version="v$MALOJA_RELEASE"
LABEL maintainer="Jonathan Boeckel <jonathanboeckel1996@gmail.com>"

WORKDIR /usr/src/app

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    libxml2-dev \
    libxslt-dev \
    py3-pip \
    libc-dev \
    linux-headers \
    && \
    pip3 install psutil && \
    pip3 install malojaserver==$MALOJA_RELEASE && \
    apk del .build-deps

EXPOSE 42010

ENTRYPOINT maloja run
